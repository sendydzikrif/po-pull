public class Elevator
{
	private boolean doorOpen=false;
	private int currentFloor = 1;
	private final int TOP_FLOOR = 5;
	private final int BOTTOM_FLOOR = 1;
	private void openDoor()
	{
		System.out.println("Opening Door.");
		doorOpen = true;
		System.out.println("Door Is Open.");
	}
	private void closeDoor()
	{
		System.out.println("Closing Door.");
		doorOpen = false;
		System.out.println("Door is Closed.");
	}
	private void goUp()
	{
		System.out.println("Going Up one floor.");
		currentFloor++;
		System.out.println("Floor : "+currentFloor);
	}
	private void goDown()
	{
		System.out.println("Going down one Floor.");
		currentFloor--;
		System.out.println("Floor : "+currentFloor);
	}
	private void setFloor(int desiredFloor)
	{
		while(currentFloor != desiredFloor)
		{
			if(currentFloor < desiredFloor)
			{
				goUp();
			}else{
				goDown();
			}
		}
	}
	private int getFloor()
	{
		return currentFloor;
	}
	private boolean checkDoorStatus()
	{
		return doorOpen;
	}
}